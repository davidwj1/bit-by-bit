import Modal from "react-modal";
import PropTypes from "prop-types";
import styled from "styled-components";

const AddTaskModal = ({
  isModalOpen,
  closeModal,
  addTask,
  handleName,
  handleDesc,
}) => {
  return (
    <Modal
      isOpen={isModalOpen}
      ariaHideApp={false}
      style={{
        overlay: {
          height: "50%",
          width: "35%",
          left: "40%",
          top: "20%",
          backgroundColor: "#335B41",
          boxShadow: "-10px 10px 5px",
          borderRadius: "20px",
        },
        content: { backgroundColor: "#7989A4", borderRadius: "50px" },
      }}
    >
      <AddForm>
        <ButtonCloseModal onClick={closeModal}>Close Window</ButtonCloseModal>
        <h1>Add New Task</h1>
        <InputTaskName
          placeholder="Task Name"
          onChange={handleName}
        ></InputTaskName>
        <InputTaskDescription
          placeholder="Task Description"
          onChange={handleDesc}
        ></InputTaskDescription>
        <ButtonAddTask onClick={addTask}>Add New Task</ButtonAddTask>
      </AddForm>
    </Modal>
  );
};

const AddForm = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 20px;
`;

const ButtonCloseModal = styled.button`
  align-self: flex-end;
  font-size: 15px;
  cursor: pointer;
  user-select: none;
`;

const InputTaskName = styled.input`
  border-radius: 15px;
  font-size: 30px;
`;

const InputTaskDescription = styled.input`
  border-radius: 15px;
  font-size: 30px;
`;

const ButtonAddTask = styled.button`
  border-radius: 15px;
  font-size: 30px;
  cursor: pointer;
  background-color: #00ff00;
  user-select: none;
  &:hover {
    background-color: #009900;
    color: lightgray;
  }
`;

export default AddTaskModal;

AddTaskModal.propTypes = {
  isModalOpen: PropTypes.bool,
  closeModal: PropTypes.instanceOf(Function),
};
