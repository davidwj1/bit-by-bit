import * as React from "react";
import styled from "styled-components";

const Logout = ({ onLogout }) => {
  return (
    <LogoutButton onClick={onLogout}>
      Logout 
    </LogoutButton>
  );
};

const LogoutButton = styled.button``;

export default Logout;
