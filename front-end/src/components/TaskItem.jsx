import styled from "styled-components";
import PropTypes from "prop-types";

const TaskItem = ({ task, editTask, removeTask, completeTask }) => {
  return (
    <TaskContainer
      onDoubleClick={() => editTask(task)}
      complete={task.complete}
    >
      <TaskName>{task.name}</TaskName>
      <TaskDescription>{task.description}</TaskDescription>
      <TaskComplete>Complete: {task.complete.toString()}</TaskComplete>
      <ButtonsContainer>
        <EditButton onClick={() => editTask(task)}>✏️</EditButton>
        <CompleteButton onClick={() => completeTask(task.id)}>
          ✔️
        </CompleteButton>
        <DeleteButton onClick={() => removeTask(task.id, "currentTasks")}>
          🗑️
        </DeleteButton>
      </ButtonsContainer>
    </TaskContainer>
  );
};

const TaskContainer = styled.div`
  transform: scale(0.9);
  display: flex;
  flex-direction: column;
  word-wrap: break-word;
  padding: 25px;
  background-color: ${({ complete }) =>
    complete === false ? "#395756" : "#395100"};
  text-decoration: ${({ complete }) =>
    complete === true ? "line-through" : "none"};
  cursor: pointer;
  &:hover {
    background-color: #7261a3;
  }
`;

const TaskName = styled.h2`
  border: 7px dotted black;
  border-left-style: none;
  border-right-style: solid;
  border-top-style: none;
  border-bottom-style: solid;
  color: white;
`;
const TaskDescription = styled.p`
  color: white;
`;
const TaskComplete = styled.p`
  color: white;
`;

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  gap: 15px;
`;

const BaseButton = styled.button`
  font-size: 25px;
  border-radius: 25px;
  width: 80%;
  user-select: none;
`;

const EditButton = styled(BaseButton)`
  background-color: orange;
  align-self: flex-end;
  &:hover {
    background-color: yellow;
  }
`;

const CompleteButton = styled(BaseButton)`
  background-color: green;
  align-self: center;
  &:hover {
    background-color: lightgreen;
  }
`;

const DeleteButton = styled(BaseButton)`
  background-color: hsl(360 100% 31%);
  width: 25%;
  &:hover {
    background-color: hsl(360 100% 58%);
  }
`;

export default TaskItem;

TaskItem.propTypes = {
  task: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  }),
};
