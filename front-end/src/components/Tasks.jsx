import List from "./List";
import PropTypes from "prop-types";
import { useState, useEffect } from "react";
import styled from "styled-components";
import UpdateTaskModal from "./UpdateTaskModal";
import AddTaskModal from "./AddTaskModal";
import {
  fetchAllTasks,
  fetchAddTask,
  fetchDeleteTask,
  fetchUpdateTask,
} from "../services/api-calls";

const Tasks = () => {
  const [taskId, setTaskId] = useState("");
  const [taskName, setTaskName] = useState("");
  const [taskDescription, setTaskDescription] = useState("");
  const [updateModalIsOpen, setUpdateModalOpen] = useState(false);
  const [addModalIsOpen, setAddModalOpen] = useState(false);
  const [taskList, setTasks] = useState([]);

  useEffect(() => {
    fetchAllTasks(setTasks);
  }, []);

  const getRandomID = () => {
    return new Date().valueOf();
  };

  const addNewTask = async () => {
    const newTask = {
      id: getRandomID(),
      name: taskName,
      description: taskDescription,
      complete: false,
    };
    const updatedList = [newTask, ...taskList];

    await fetchAddTask(newTask);

    setTasks(updatedList);
    closeAddModal();
  };

  const removeTask = async (id) => {
    const updatedList = taskList.filter((item) => item.id !== id);
    await fetchDeleteTask(id);
    setTasks(updatedList);
  };

  const completeTask = async (id) => {
    const index = taskList.findIndex((obj) => obj.id === id);
    taskList[index].complete = true;

    //reflects changes, extract later so other functions can use
    const updatedList = taskList.map((item) => item);
    setTasks(updatedList);

    const currentTask = {
      id: id,
    };
    const updatedTask = {
      name: taskList[index].name,
      description: taskList[index].description,
      complete: true,
    };
    await fetchUpdateTask(currentTask, updatedTask);
  };

  const updateTask = async () => {
    const index = taskList.findIndex((obj) => obj.id === taskId);
    taskList[index].name = taskName;
    taskList[index].description = taskDescription;

    const currentTask = {
      id: taskId,
    };
    const updatedTask = {
      name: taskName,
      description: taskDescription,
    };
    await fetchUpdateTask(currentTask, updatedTask);
    closeModal();
  };

  const handleOnChange = ({ target: { value } }) => {
    setTaskName(value);
  };

  const handleOnChangeDesc = ({ target: { value } }) => {
    setTaskDescription(value);
  };

  const editTask = (task) => {
    setTaskId(task.id);
    setTaskName(task.name);
    setTaskDescription(task.description);
    setUpdateModalOpen(true);
  };

  const openAddModal = () => {
    setAddModalOpen(true);
  };

  const closeAddModal = () => {
    setAddModalOpen(false);
  };

  const clearInputFields = () => {
    setTaskId("");
    setTaskName("");
    setTaskDescription("");
  };

  const closeModal = () => {
    clearInputFields();
    setUpdateModalOpen(false);
  };

  return (
    <TaskContainer>
      <List
        tasksList={taskList}
        removeTask={removeTask}
        completeTask={completeTask}
        editTask={editTask}
      />
      <AddButton type="submit" onClick={openAddModal}>
        +
      </AddButton>
      <AddTaskModal
        isModalOpen={addModalIsOpen}
        addTask={addNewTask}
        closeModal={closeAddModal}
        handleName={handleOnChange}
        handleDesc={handleOnChangeDesc}
      ></AddTaskModal>

      <UpdateTaskModal
        isModalOpen={updateModalIsOpen}
        updateTask={updateTask}
        closeModal={closeModal}
        handleTask={handleOnChange}
        handleDesc={handleOnChangeDesc}
        taskName={taskName}
        taskDesc={taskDescription}
      ></UpdateTaskModal>
    </TaskContainer>
  );
};

const TaskContainer = styled.div`
  display: flex;
  height: 50px;
  //border: 5px solid red;
`;

const AddButton = styled.button`
  background-color: green;
  border-radius: 50px;
  width: 100px;
  box-shadow: 0px 8px 5px;
  user-select: none;
  &:hover {
    background-color: lightgreen;
  }
`;

export default Tasks;

Tasks.propTypes = {
  id: PropTypes.number,
  title: PropTypes.string,
};
