import * as React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import TaskItem from "./TaskItem";

const List = ({ tasksList, removeTask, completeTask, editTask }) => {
  return (
    <ListContainer>
      <ListTitle>Tasks To Do</ListTitle>
      <ListInstructions>Double click task to edit</ListInstructions>
      <TaskListContainer>
        {tasksList.map((task) => (
          <TaskItem
            key={task.id}
            task={task}
            removeTask={removeTask}
            completeTask={completeTask}
            editTask={editTask}
          ></TaskItem>
        ))}
      </TaskListContainer>
    </ListContainer>
  );
};

const ListContainer = styled.div`
  //border: 5px solid blue;
  text-align: center;
  width: 700px;
`;

const ListTitle = styled.h2`
  text-align: left;
  user-select: none;
`;

const ListInstructions = styled.p`
  text-align: left;
  font-style: italic;
  user-select: none;
`;

const TaskListContainer = styled.div`
  background-color: #1b3022;
  border: 5px solid black;
  overflow-y: auto;
  max-height: 600px;
  height: 600px;
`;

export default List;

List.propTypes = {
  tasksList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      complete: PropTypes.bool,
    })
  ),
};

List.defaultProps = {
  id: 991,
  name: "Task 1",
};
