import Modal from "react-modal";
import PropTypes from "prop-types";
import styled from "styled-components";

const UpdateTaskModal = ({
  isModalOpen,
  updateTask,
  closeModal,
  handleTask,
  handleDesc,
  taskName,
  taskDesc,
}) => {
  return (
    <Modal
      isOpen={isModalOpen}
      ariaHideApp={false}
      style={{
        overlay: {
          height: "50%",
          width: "35%",
          left: "40%",
          top: "20%",
          backgroundColor: "#335B41",
          boxShadow: "-10px 10px 5px",
          borderRadius: "20px",
        },
        content: { backgroundColor: "#7989A4", borderRadius: "50px" },
      }}
    >
      <UpdateForm>
        <ButtonCloseModal onClick={closeModal}>Close Window</ButtonCloseModal>
        <h1>Update Task Details</h1>
        <InputTaskName
          placeholder="Task Name"
          onChange={handleTask}
          value={taskName}
        ></InputTaskName>
        <InputTaskDescription
          placeholder="Task Description"
          onChange={handleDesc}
          value={taskDesc}
        ></InputTaskDescription>
        <ButtonSaveTask onClick={updateTask}>Save</ButtonSaveTask>
      </UpdateForm>
    </Modal>
  );
};

const UpdateForm = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 20px;
`;

const ButtonCloseModal = styled.button`
  align-self: flex-end;
  font-size: 15px;
  cursor: pointer;
  user-select: none;
`;

const InputTaskName = styled.input`
  border-radius: 15px;
  font-size: 30px;
`;

const InputTaskDescription = styled.input`
  border-radius: 15px;
  font-size: 30px;
`;

const ButtonSaveTask = styled.button`
  border-radius: 15px;
  font-size: 30px;
  cursor: pointer;
  background-color: #00ff00;
  user-select: none;
  &:hover {
    background-color: #009900;
    color: lightgray;
  }
`;

export default UpdateTaskModal;

UpdateTaskModal.propTypes = {
  isModalOpen: PropTypes.bool,
  updateTask: PropTypes.func,
};
