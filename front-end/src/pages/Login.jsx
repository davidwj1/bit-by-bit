import { useState } from "react";
import styled from "styled-components";

const Login = ({ onLogin }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  function handleSubmit(event) {
    event.preventDefault();
    onLogin({ username, password });
  }

  return (
    <FormContainer onSubmit={handleSubmit}>
      <PageHeader>Bit By Bit - Login</PageHeader>
      <InputFields
        type="text"
        placeholder="Username"
        value={username}
        onChange={(e) => setUsername(e.target.value)}
      ></InputFields>
      <InputFields
        type="password"
        placeholder="Password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      ></InputFields>
      <LoginButton type="submit" value="Login"></LoginButton>
    </FormContainer>
  );
};

const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
  height: 100vh;
  background-color: gray;
  position: absolute;
  top: 0px;
  bottom: 0px;
  left: 0px;
  right: 0px;
  padding: 0px 50px;
`;

const PageHeader = styled.h1``;

const InputFields = styled.input`
  height: 64px;
  max-width: 50%;
`;

const LoginButton = styled.input`
  height: 64px;
  max-width: 25%;
`;

export default Login;
