import PropTypes from "prop-types";
import styled from "styled-components";

const Error = ({ errorCode, errorMessage }) => {
  errorCode = 123;
  errorMessage = "Hello";
  return (
    <ErrorContainer>
      <ErrorHeader>
        Error {errorCode}: {errorMessage}
      </ErrorHeader>
    </ErrorContainer>
  );
};

const ErrorContainer = styled.div`
  position: absolute;
  top: 0px;
  bottom: 0px;
  left: 0px;
  right: 0px;
  padding: 0px 50px;
  background-color: gray;
`;

const ErrorHeader = styled.h1``;

export default Error;

Error.propTypes = {
  errorMessage: PropTypes.string,
  errorCode: PropTypes.number,
};
