import { useState } from "react";
import styled from "styled-components";

const Register = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");

  const handleSubmit = () => {
    // register api call
    //
  };

  const checkPassword = () => {
    // if length same && password2 != password, error
    // else fine
  }

  return (
    <FormContainer onSubmit={handleSubmit}>
      <PageHeader>Bit By Bit - Register</PageHeader>
      <InputFields
        type="text"
        placeholder="Username"
        value={username}
        onChange={(e) => setUsername(e.target.value)}
      ></InputFields>
      <InputFields
        type="password"
        placeholder="Password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      ></InputFields>
      <InputFields
        type="password"
        placeholder="Confirm Password"
        value={password2}
        onChange={(e) => setPassword2(e.target.value)}
      ></InputFields>
      <LoginButton type="submit" value="Register"></LoginButton>
    </FormContainer>
  );
};

export default Register;

const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
  height: 100vh;
  background-color: gray;
  position: absolute;
  top: 0px;
  bottom: 0px;
  left: 0px;
  right: 0px;
  padding: 0px 50px;
`;

const PageHeader = styled.h1``;

const InputFields = styled.input`
  height: 64px;
  max-width: 50%;
`;

const LoginButton = styled.input`
  height: 64px;
  max-width: 25%;
`;
