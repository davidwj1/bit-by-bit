import { CookiesProvider, useCookies } from "react-cookie";
import styled from "styled-components";
import Logout from "../components/Logout";
import Tasks from "../components/Tasks";
import Login from "./Login";

const Home = () => {
  const [cookies, setCookie, removeCookie] = useCookies(["cookie"]);
  const callApiAuth = async () => {};

  const handleLogin = (user) => {
    // const passwordHash = bycrypt.hashSync(user.password, 14);
    // send username and hash to api auth
    // if get back success from api then setcookie
    // set cookie with the value from api call
    setCookie("cookie", { token: Date.now() }, { path: "/", maxAge: 30 });
  };

  const handleLogout = (user) => {
    //
    removeCookie("cookie");
  };

  return (
    <CookiesProvider>
      <HomeContainer>
        <HeaderTitle>Bit by Bit 📝</HeaderTitle>
        <Logout onLogout={handleLogout} />
        {cookies.cookie ? <Tasks /> : <Login onLogin={handleLogin} />}
      </HomeContainer>
    </CookiesProvider>
  );
};

const HomeContainer = styled.div`
  background-color: #4f5d75;
  position: absolute;
  top: 0px;
  bottom: 0px;
  left: 0px;
  right: 0px;
  padding: 0px 50px;
`;

const HeaderTitle = styled.h1`
  font-size: 50px;
  user-select: none;
`;

export default Home;
