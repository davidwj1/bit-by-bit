const fetchAllTasks = async (setTasks) => {
  const request = new Request("http://localhost:5000/api/task");

  const options = {
    method: "GET",
  };

  fetch(request, options)
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      return response.json();
    })
    .then((response) => {
      setTasks(response.tasks);
    });
};

const fetchAddTask = async (newTask) => {
  const request = new Request("http://localhost:5000/api/task");
  const options = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id: newTask.id,
      name: newTask.name,
      description: newTask.description,
    }),
  };
  fetch(request, options).then((response) => {
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    return response;
  });
};

const fetchDeleteTask = async (idToDelete) => {
  const request = new Request(`http://localhost:5000/api/task/${idToDelete}`);
  const options = {
    method: "DELETE",
  };
  fetch(request, options).then((response) => {
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    return response;
  });
};

const fetchUpdateTask = async (taskToUpdate, updateTask) => {
  const request = new Request(
    `http://localhost:5000/api/task/${taskToUpdate.id}`
  );
  const options = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      specifiedTask: {
        id: taskToUpdate.id,
      },
      updatedName: updateTask.name,
      updatedDescription: updateTask.description,
      updatedComplete: updateTask.complete,
    }),
  };

  fetch(request, options).then((response) => {
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    return response;
  });
};

export { fetchAllTasks, fetchAddTask, fetchDeleteTask, fetchUpdateTask };
