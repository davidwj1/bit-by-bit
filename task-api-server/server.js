const ServerApp = require("./src/server-api");
const MongoRepository = require("./src/repository/mongo-storage");
const InMemory = require("./src/repository/in-memory-storage");
const process = require("process");
const dotenv = require('dotenv')
dotenv.config()

let server
if (process.env.DATABASE === "mongo") {
  server = new ServerApp(new MongoRepository());
  server.start();
} else {
  server = new ServerApp(new InMemory());
  server.start();
}

process.on("SIGINT", () => {
  server.stop();
});

process.on("SIGTERM", () => {
  server.stop();
});
