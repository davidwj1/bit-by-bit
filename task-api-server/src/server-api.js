const express = require("express");
const bodyParser = require("body-parser");
const halson = require("halson");
const Task = require("./repository/task");
const cors = require("cors");
const TaskAPI = require("./task-api");
const UserAPI = require("./auth-api");
const sessions = require("client-sessions");

const InMemoryStorage = require("./repository/in-memory-storage") //replace

class ServerApp {
  constructor(taskRepository) {
    this.taskRepository = taskRepository;
    this.app = express();
    this.app.use(bodyParser.json());
    this.app.use(cors());
    this.taskAPI = new TaskAPI(taskRepository);

    this.app.use(
      sessions({
        cookieName: "session",
        secret: "secret",
        duration: 30 * 60 * 1000,
        activeDuration: 5 * 60 * 1000,
        httpOnly: true,
        secure: true,
        ephemeral: true
      })
    );

    const inMemory = new InMemoryStorage(); //replace
    this.userAPI = new UserAPI(inMemory)
    //routers to use /api
    this.app.get("/api/task", async (req, res) => {
      if ("name" in req.query) {
        const taskResource = await this.handleGetAllWithName(req.query.name);
        res.status(200).json(taskResource);
      } else {
        const taskList = await this.handleGetAll();
        const taskResource = this.addCreateHalsonLinks(taskList);
        res.status(200).json(taskResource);
      }
    });

    this.app.get("/api/task/:id", async (req, res) => {
      const task = await this.taskAPI.getSpecificTask(req.params.id);
      if (task === null || JSON.stringify(task) === "{}") {
        const hal = this.addCreateHalsonLinks(task);
        res.json(hal);
      } else {
        const hal = this.addUpdateDeleteHalsonLinks(task);
        res.json(hal);
      }
    });

    this.app.post("/api/task", async (req, res) => {
      if ("name" in req.body && "description" in req.body) {
        const taskName = req.body.name;
        const taskDescription = req.body.description;
        const taskId = req.body.id; //Used for testing purposes
        const newTask = new Task(taskName, taskDescription, taskId);
        await this.taskAPI.createTask(newTask);
        res.status(201).send("Created new task");
      } else {
        res.status(400).send({ error: "Invalid Body" });
      }
    });

    this.app.put("/api/task/:id", async (req, res) => {
      const updates = {
        name: req.body.updatedName,
        description: req.body.updatedDescription,
        complete: req.body.updatedComplete,
      };
      const apiResponse = await this.taskAPI.updateTask(
        req.body.specifiedTask,
        updates
      );
      apiResponse === "success"
        ? res.status(200).send({ error: "Successful Update" })
        : res.status(400).send({ error: "Failed Update" });
    });

    this.app.delete("/api/task/:id", async (req, res) => {
      const apiResponse = await this.taskAPI.deleteTask(req.params.id);
      apiResponse
        ? res.status(204).send()
        : res.status(400).send({ error: "Invalid Task" });
    });

    this.app.post("/auth/register", async (req, res) => {
      const response = await this.userAPI.createUser(req.body)
      if (response.status === "User Exists") {
        res.status(400).send({ status: "User Exists" })
      } else {
        res.status(200).send({ status: "Success" })
      }
    })

    this.app.post("/auth/login", async (req, res) => {
      const response = await this.userAPI.loginUser(req.body)
      if (response.status === "LoggedIn") {
        console.log(req.session);
        res.status(200).send({ status: "Logged In" })
      } else {
        res.status(400).send({ status: "Failed Log In" })
      }
    })
  }

  //handleCreatingTasks
  //handleReadingTasks
  //handleErrorResponses

  handleErrorResponses() { }

  addSelfHalsonLinks(task) {
    const hal = halson(task);
    hal.addLink("self", `/api/task/${task.id}`);
    return hal;
  }

  addCreateHalsonLinks(tasks) {
    const hal = halson(tasks);
    hal.addLink("self", `/api/task`);
    hal.addLink("create", { href: "/api/task", method: "POST" });
    return hal;
  }

  addUpdateDeleteHalsonLinks(task) {
    const hal = halson(task);
    hal.addLink("self", `/api/task/${task.id}`);
    hal.addLink("update", {
      href: `/api/task/${task.id}`,
      method: "PUT",
    });
    hal.addLink("delete", {
      href: `/api/task/${task.id}`,
      method: "DELETE",
    });
    return hal;
  }

  async handleGetAllWithName(nameQuery) {
    const allTasks = await this.taskAPI.searchByName(nameQuery);
    const tasks = [];
    if (allTasks.length !== 0) {
      allTasks.forEach((task) => {
        tasks.push(this.addSelfHalsonLinks(task));
      });
    } else {
      return this.addCreateHalsonLinks({ tasks: [] });
    }
    return tasks;
  }

  async handleGetAll() {
    const allTasks = await this.taskAPI.getTasks();
    const tasks = [];

    allTasks.tasks.forEach((task) => {
      tasks.push(this.addSelfHalsonLinks(task));
    });
    return { tasks };
  }

  async start() {
    await this.taskRepository.setup();
    const serverPromise = new Promise((resolve, reject) => {
      this.server = this.app.listen(5000, async () => {
        console.log("Server API is running on port 5000");
        resolve();
      });
    });
    return serverPromise;
  }

  async stop() {
    await this.taskRepository.cleanUp();
    const serverPromise = new Promise((resolve, reject) => {
      this.server.close(async () => {
        console.log("\nServer is now closed");
        resolve();
      });
    });
    return serverPromise;
  }
}

module.exports = ServerApp;
