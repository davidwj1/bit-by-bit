const userAPI = require("../auth-api");
const InMemoryTaskStorage = require("../repository/in-memory-storage.js");

describe("auth-api", () => {
    let api;
    beforeEach(() => {
        const inMemory = new InMemoryTaskStorage();
        api = new userAPI(inMemory);
    });

    describe("register user", () => {
        it("new user", async () => {
            const username = "Bob"
            const password = "password"
            const response = await api.createUser({ username, password })
            expect(response).toEqual({ id: expect.any(String), username })
        });
        it("existing user", async () => {
            const username = "Bob"
            const password = "password"
            await api.createUser({ username, password })
            const response = await api.createUser({ username, password })
            expect(response).toEqual({ status: "User Exists" })
        });
    });

    describe("login", () => {
        it("existing user with correct password", async () => {
            const username = "Bob"
            const password = "password"
            await api.createUser({ username, password })
            const response = await api.loginUser({ username, password })
            expect(response).toEqual({ id: expect.any(String), username, status: "LoggedIn" })
        });
        it("non-existent user", async () => {
            const response = await api.loginUser({ username: "iAmNotReal", password: "abcd" })
            expect(response).toEqual({})
        });
        it("existing user with incorrect password", async () => {
            const username = "Bob"
            const password = "password"
            await api.createUser({ username, password })
            const response = await api.loginUser({ username: "Bob", password: "abcd" })
            expect(response).toEqual({ status: "Unsuccessful Login" })
        });
    });
});