const InMemoryTaskRepository = require("../repository/in-memory-storage.js");
const Task = require("../repository/task.js");

describe("In memory repository Tests", () => {
  let taskRepository;
  beforeEach(() => {
    taskRepository = new InMemoryTaskRepository();
  });

  const createTasks = async (numTimesToCreate) => {
    for (let ii = 1; ii <= numTimesToCreate; ii++) {
      await taskRepository.create(new Task(`Test${ii}`, `Desc${ii}`, `${ii}`));
    }
  };

  describe("Creating Tasks", () => {
    it("Create 1 task", async () => {
      await createTasks(1);
      const data = await taskRepository.getAll();
      expect(data).toEqual({
        tasks: [
          {
            name: "Test1",
            description: "Desc1",
            id: expect.any(String),
            complete: false,
          },
        ],
      });
    });
    it("Create 3 tasks", async () => {
      await createTasks(3);
      const data = await taskRepository.getAll();
      expect(data).toEqual({
        tasks: [
          {
            name: "Test1",
            description: "Desc1",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "Test2",
            description: "Desc2",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "Test3",
            description: "Desc3",
            id: expect.any(String),
            complete: false,
          },
        ],
      });
    });
  });
  describe("Getting Tasks", () => {
    it("Get all tasks", async () => {
      await createTasks(3);
      const data = await taskRepository.getAll();
      expect(data).toEqual({
        tasks: [
          {
            name: "Test1",
            description: "Desc1",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "Test2",
            description: "Desc2",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "Test3",
            description: "Desc3",
            id: expect.any(String),
            complete: false,
          },
        ],
      });
    });
    it("Get 2nd task out of 3", async () => {
      await createTasks(3);
      const specifiedData = await taskRepository.getById(2);
      expect(specifiedData).toEqual({
        name: "Test2",
        description: "Desc2",
        id: expect.any(String),
        complete: false,
      });
    });
    it("Getting non-existent task", async () => {
      const response = await taskRepository.getById("99999");
      expect(response).toEqual({});
    });
    it("Search matching 2 task by name", async () => {
      await createTasks(3);
      const response = await taskRepository.searchByName("Test1");
      expect(response).toEqual([
        {
          name: "Test1",
          description: "Desc1",
          id: expect.any(String),
          complete: false,
        },
      ]);
    });
    it("Search matching 2 tasks by name", async () => {
      await createTasks(3);
      await taskRepository.create(new Task("Test1", "Desc123"));
      const response = await taskRepository.searchByName("Test1");
      expect(response).toEqual([
        {
          name: "Test1",
          description: "Desc1",
          id: expect.any(String),
          complete: false,
        },
        {
          name: "Test1",
          description: "Desc123",
          id: expect.any(String),
          complete: false,
        },
      ]);
    });
  });

  describe("Updating Tasks", () => {
    it("Update a task name", async () => {
      await createTasks(1);
      const specifiedTask = await taskRepository.getById(1);
      await taskRepository.update(specifiedTask, { name: "TestChange" });
      const response = await taskRepository.getById(1);
      expect(response).toEqual({
        name: "TestChange",
        description: expect.any(String),
        id: expect.any(String),
        complete: false,
      });
    });
    it("Update a task description", async () => {
      await createTasks(1);
      const specifiedTask = await taskRepository.getById(1);
      await taskRepository.update(specifiedTask, {
        description: "changedDescription",
      });
      const response = await taskRepository.getById(1);
      expect(response).toEqual({
        name: expect.any(String),
        description: "changedDescription",
        id: expect.any(String),
        complete: false,
      });
    });
    it("Update a task name and description", async () => {
      await createTasks(1);
      const specifiedTask = await taskRepository.getById(1);
      await taskRepository.update(specifiedTask, {
        name: "changedName",
        description: "changedDescription",
      });
      const response = await taskRepository.getById(1);
      expect(response).toEqual({
        name: "changedName",
        description: "changedDescription",
        id: expect.any(String),
        complete: false,
      });
    });
    it("update a task complete status", async () => {
      await createTasks(1);
      const specifiedTask = await taskRepository.getById(1);
      await taskRepository.update(specifiedTask, {
        complete: true,
      });
      const response = await taskRepository.getById(1);
      expect(response).toEqual({
        name: "Test1",
        description: "Desc1",
        id: expect.any(String),
        complete: true,
      });
    });
  });

  describe("Deleting Tasks", () => {
    it("Delete 1 task out of list of 1", async () => {
      await createTasks(1);
      const specifiedTask = await taskRepository.getById(1);
      await taskRepository.delete(specifiedTask.id);
      const response = await taskRepository.getAll();
      expect(response).toEqual({ tasks: [] });
    });
    it("Delete 1st task out of list of 3", async () => {
      await createTasks(3);
      const specifiedTask = await taskRepository.getById(1);
      await taskRepository.delete(specifiedTask.id);
      const response = await taskRepository.getAll();
      expect(response).toEqual({
        tasks: [
          {
            name: "Test2",
            description: "Desc2",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "Test3",
            description: "Desc3",
            id: expect.any(String),
            complete: false,
          },
        ],
      });
    });
  });
});
