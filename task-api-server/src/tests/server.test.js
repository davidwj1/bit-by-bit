const Task = require("../repository/task.js");
const InMemoryTaskStorage = require("../repository/in-memory-storage.js");
const mongoStorage = require("../repository/mongo-storage.js");
const request = require("supertest");
const ServerApp = require("../server-api.js");

describe("Task API", () => {
  let server;
  let taskRepository;
  beforeEach(async () => {
    //const taskRepository = new mongoStorage();
    const taskRepository = new InMemoryTaskStorage();
    server = new ServerApp(taskRepository);
    await server.start();
  });

  afterEach(async () => {
    await server.stop();
  });

  //Helper test function to create tasks
  const createTasks = async (numTimesToRepeat) => {
    for (let ii = 1; ii <= numTimesToRepeat; ii++) {
      await request(server.app)
        .post("/api/task")
        .send(new Task(`Test${ii}`, `Desc${ii}`, `${ii}`));
    }
  };

  describe("GET Tasks", () => {
    it("Responds with status code 200", async () => {
      const response = await request(server.app).get("/api/task");
      expect(response.statusCode).toBe(200);
    });
    it("Responds with JSON", async () => {
      const responseData = await request(server.app).get("/api/task");
      expect(responseData.type).toBe("application/json");
    });
    it("Responds with empty list", async () => {
      const responseData = await request(server.app).get("/api/task");
      expect(responseData.body).toEqual({
        _links: {
          self: { href: "/api/task" },
          create: { href: "/api/task", method: "POST" },
        },
        tasks: [],
      });
    });
    it("Responds with list of tasks", async () => {
      await createTasks(3);
      const responseData = await request(server.app).get("/api/task");
      expect(responseData.body).toEqual({
        _links: {
          self: { href: "/api/task" },
          create: { href: "/api/task", method: "POST" },
        },
        tasks: [
          {
            name: "Test1",
            description: "Desc1",
            id: "1",
            complete: false,
            _links: { self: { href: "/api/task/1" } },
          },
          {
            name: "Test2",
            description: "Desc2",
            id: "2",
            complete: false,
            _links: { self: { href: "/api/task/2" } },
          },
          {
            name: "Test3",
            description: "Desc3",
            id: "3",
            complete: false,
            _links: { self: { href: "/api/task/3" } },
          },
        ],
      });
    });
    describe("Get Task with ID", () => {
      it("Responds with 1 task out of list of 1", async () => {
        await createTasks(1);
        const specifiedResourceData = await request(server.app).get(
          `/api/task/1`
        );
        expect(specifiedResourceData.body).toEqual({
          name: "Test1",
          description: "Desc1",
          id: "1",
          complete: false,
          _links: {
            self: { href: "/api/task/1" },
            update: { href: "/api/task/1", method: "PUT" },
            delete: { href: "/api/task/1", method: "DELETE" },
          },
        });
      });
      it("Responds with 2nd task out of list of 3", async () => {
        await createTasks(3);
        const specifiedResourceData = await request(server.app).get(
          `/api/task/2`
        );
        expect(specifiedResourceData.body).toEqual({
          name: "Test2",
          description: "Desc2",
          id: "2",
          complete: false,
          _links: {
            self: { href: "/api/task/2" },
            update: { href: "/api/task/2", method: "PUT" },
            delete: { href: "/api/task/2", method: "DELETE" },
          },
        });
      });
      it("Return empty when searching for non-existing id", async () => {
        const specifiedResourceData = await request(server.app).get(
          `/api/task/999`
        );
        expect(specifiedResourceData.body).toEqual({
          _links: {
            self: { href: "/api/task" },
            create: { href: "/api/task", method: "POST" },
          },
        });
      });
    });
    describe("Search task with name", () => {
      it("Responds with 1 matching task out of list of 3", async () => {
        await createTasks(3);
        const response = await request(server.app)
          .get("/api/task")
          .query({ name: "Test2" });
        expect(response.body).toEqual([
          {
            name: "Test2",
            description: "Desc2",
            id: expect.any(String),
            complete: false,
            _links: { self: { href: "/api/task/2" } },
          },
        ]);
      });
      it("Responds with 2 matching tasks out of list of 3", async () => {
        await request(server.app)
          .post("/api/task")
          .send(new Task("Test1", "Desc1", "1"));
        await request(server.app)
          .post("/api/task")
          .send(new Task("Test1", "Desc123", "2"));
        await request(server.app)
          .post("/api/task")
          .send(new Task("Test2", "Desc2", "3"));

        const response = await request(server.app)
          .get("/api/task")
          .query({ name: "Test1" });

        expect(response.body).toEqual([
          {
            name: "Test1",
            description: "Desc1",
            id: expect.any(String),
            complete: false,
            _links: { self: { href: "/api/task/1" } },
          },
          {
            name: "Test1",
            description: "Desc123",
            id: expect.any(String),
            complete: false,
            _links: { self: { href: "/api/task/2" } },
          },
        ]);
      });
      it("Search non matching tasks out of list of 3", async () => {
        await createTasks(3);
        const response = await request(server.app)
          .get("/api/task")
          .query({ name: "Non exist" });
        expect(response.body).toEqual({
          tasks: [],
          _links: {
            self: { href: "/api/task" },
            create: { href: "/api/task", method: "POST" },
          },
        });
      });
      it("Search with invalid query", async () => {
        const response = await request(server.app)
          .get("/api/task")
          .query({ thisdoesntwork: "Non exist" });
        expect(response.body).toEqual({
          tasks: [],
          _links: {
            self: { href: "/api/task" },
            create: { href: "/api/task", method: "POST" },
          },
        });
      });
    });
  });
  describe("POST Tasks", () => {
    it("Creates 1 new task", async () => {
      await createTasks(1);
      const responseData = await request(server.app).get("/api/task");
      expect(responseData.body).toEqual({
        _links: {
          self: { href: "/api/task" },
          create: { href: "/api/task", method: "POST" },
        },
        tasks: [
          {
            name: "Test1",
            description: "Desc1",
            id: expect.any(String),
            complete: false,
            _links: { self: { href: "/api/task/1" } },
          },
        ],
      });
    });
    it("Creates 2 new tasks", async () => {
      await createTasks(2);
      const responseData = await request(server.app).get("/api/task");
      expect(responseData.body).toEqual({
        _links: {
          self: { href: "/api/task" },
          create: { href: "/api/task", method: "POST" },
        },
        tasks: [
          {
            name: "Test1",
            description: "Desc1",
            id: expect.any(String),
            complete: false,
            _links: { self: { href: "/api/task/1" } },
          },
          {
            name: "Test2",
            description: "Desc2",
            id: expect.any(String),
            complete: false,
            _links: { self: { href: "/api/task/2" } },
          },
        ],
      });
    });
    it("Create new task with invalid object", async () => {
      const createResponse = await request(server.app)
        .post("/api/task")
        .send({ invalid: "Hello" });
      expect(createResponse.status).toBe(400);
      expect(createResponse.body).toEqual({ error: "Invalid Body" });
    });
  });
  describe("PUT Tasks", () => {
    it("Update task in list of 1", async () => {
      await createTasks(1);
      const specifiedTask = (await request(server.app).get(`/api/task/1`)).body;
      await request(server.app).put(`/api/task/1`).send({
        specifiedTask,
        updatedName: "changedName",
        updatedDescription: "changedDescription",
        updatedComplete: true,
      });
      const response = (await request(server.app).get(`/api/task/1`)).body;
      expect(response).toEqual({
        name: "changedName",
        description: "changedDescription",
        id: expect.any(String),
        complete: true,
        _links: {
          self: { href: "/api/task/1" },
          update: { href: "/api/task/1", method: "PUT" },
          delete: { href: "/api/task/1", method: "DELETE" },
        },
      });
    });
    it("Update task name in list of 1", async () => {
      await createTasks(1);
      const specifiedTask = (await request(server.app).get(`/api/task/1`)).body;
      await request(server.app).put(`/api/task/1`).send({
        specifiedTask,
        updatedName: "changedName",
      });
      const response = (await request(server.app).get(`/api/task/1`)).body;
      expect(response).toEqual({
        name: "changedName",
        description: "Desc1",
        id: expect.any(String),
        complete: false,
        _links: {
          self: { href: "/api/task/1" },
          update: { href: "/api/task/1", method: "PUT" },
          delete: { href: "/api/task/1", method: "DELETE" },
        },
      });
    });
    it("Update task description in list of 1", async () => {
      await createTasks(1);
      const specifiedTask = (await request(server.app).get(`/api/task/1`)).body;
      await request(server.app).put(`/api/task/1`).send({
        specifiedTask,
        updatedDescription: "changedDesc",
      });
      const response = (await request(server.app).get(`/api/task/1`)).body;
      expect(response).toEqual({
        name: "Test1",
        description: "changedDesc",
        id: expect.any(String),
        complete: false,
        _links: {
          self: { href: "/api/task/1" },
          update: { href: "/api/task/1", method: "PUT" },
          delete: { href: "/api/task/1", method: "DELETE" },
        },
      });
    });
    it("Update task complete in list of 1", async () => {
      await createTasks(1);
      const specifiedTask = (await request(server.app).get(`/api/task/1`)).body;
      await request(server.app).put(`/api/task/1`).send({
        specifiedTask,
        updatedComplete: true,
      });
      const response = (await request(server.app).get(`/api/task/1`)).body;
      expect(response).toEqual({
        name: "Test1",
        description: "Desc1",
        id: expect.any(String),
        complete: true,
        _links: {
          self: { href: "/api/task/1" },
          update: { href: "/api/task/1", method: "PUT" },
          delete: { href: "/api/task/1", method: "DELETE" },
        },
      });
    });
    it("Update 2nd task in list of 3", async () => {
      await createTasks(3);
      const specifiedTask = (await request(server.app).get(`/api/task/2`)).body;
      await request(server.app).put(`/api/task/2`).send({
        specifiedTask,
        updatedName: "changedName",
        updatedDescription: "changedDescription",
        updatedComplete: true,
      });
      const response = (await request(server.app).get(`/api/task`)).body;
      expect(response).toEqual({
        _links: {
          self: { href: "/api/task" },
          create: { href: "/api/task", method: "POST" },
        },
        tasks: [
          {
            name: "Test1",
            description: "Desc1",
            id: expect.any(String),
            complete: false,
            _links: { self: { href: "/api/task/1" } },
          },
          {
            name: "changedName",
            description: "changedDescription",
            id: expect.any(String),
            complete: true,
            _links: { self: { href: "/api/task/2" } },
          },
          {
            name: "Test3",
            description: "Desc3",
            id: expect.any(String),
            complete: false,
            _links: { self: { href: "/api/task/3" } },
          },
        ],
      });
    });
    it("Update invalid task in list", async () => {
      const response = await request(server.app).put("/api/task/999").send({
        specifiedTask: {},
        updatedName: "changedName",
        updatedDescription: "changedDescription",
        updatedComplete: true,
      });
      expect(response.body).toEqual({
        error: "Failed Update",
      });
    });
  });
  describe("DELETE Tasks", () => {
    it("Deletes task in list of 1", async () => {
      await createTasks(1);
      const deleteResponse = await request(server.app).delete(`/api/task/1`);
      expect(deleteResponse.status).toBe(204);
      const responseDataAfterDeletion = await request(server.app).get(
        "/api/task"
      );
      expect(responseDataAfterDeletion.body).toEqual({
        _links: {
          self: { href: "/api/task" },
          create: { href: "/api/task", method: "POST" },
        },
        tasks: [],
      });
    });
    it("Deletes 2nd task in list of 3", async () => {
      await createTasks(3);
      const deleteResponse = await request(server.app).delete(`/api/task/2`);
      expect(deleteResponse.status).toBe(204);
      const responseDataAfterDeletion = await request(server.app).get(
        "/api/task"
      );
      expect(responseDataAfterDeletion.body).toEqual({
        tasks: [
          {
            name: "Test1",
            description: "Desc1",
            id: expect.any(String),
            complete: false,
            _links: { self: { href: "/api/task/1" } },
          },
          {
            name: "Test3",
            description: "Desc3",
            id: expect.any(String),
            complete: false,
            _links: { self: { href: "/api/task/3" } },
          },
        ],
        _links: {
          self: { href: "/api/task" },
          create: { href: "/api/task", method: "POST" },
        },
      });
    });
    it("Deletes non existing task in list", async () => {
      const deleteResponse = await request(server.app).delete("/api/task/999");
      expect(deleteResponse.status).toBe(400);
      expect(deleteResponse.body).toEqual({ error: "Invalid Task" });
    });
  });
  describe.only("Authentication", () => {
    describe("register user", () => {
      it("new user", async () => {
        const username = "Bob"
        const password = "password"
        const response = await request(server.app).post("/auth/register").send({ username, password })
        expect(response.status).toBe(200)
        expect(response.body).toEqual({ status: "Success" })
      });
      it("user already exist", async () => {
        const username = "Bob"
        const password = "password"
        await request(server.app).post("/auth/register").send({ username, password })
        const response = await request(server.app).post("/auth/register").send({ username, password })
        expect(response.status).toBe(400)
        expect(response.body).toEqual({ status: "User Exists" })
      });
    });
    describe("login user", () => {
      it("existing user with correct password", async () => {
        const username = "Bob"
        const password = "password"
        await request(server.app).post("/auth/register").send({ username, password })
        const response = await request(server.app).post("/auth/login").send({ username, password })
        expect(response.statusCode).toBe(200)
        expect(response.body).toEqual({ status: "Logged In" })
      });
      it("existing user with incorrect password", async () => {
        const username = "Bob"
        const password = "password"
        await request(server.app).post("/auth/register").send({ username, password })
        const response = await request(server.app).post("/auth/login").send({ username, password: "aaa" })
        expect(response.body).toEqual({ status: "Failed Log In" })
      });
      it("non existent user", async () => {
        const response = await request(server.app).post("/auth/login").send({ username: "iamnotreal", password: "aaa" })
        expect(response.statusCode).toBe(400)
        expect(response.body).toEqual({ status: "Failed Log In" })
      });
    });
  });
});
