const apiClass = require("../task-api");
const InMemoryTaskStorage = require("../repository/in-memory-storage");
const Task = require("../repository/task");

describe("Task API", () => {
  let api;
  beforeEach(() => {
    const inMemory = new InMemoryTaskStorage();
    api = new apiClass(inMemory);
  });

  const createTasks = async (numTimesToRepeat) => {
    for (let ii = 1; ii <= numTimesToRepeat; ii++) {
      await api.createTask(new Task(`Task${ii}`, `desc${ii}`, `${ii}`));
    }
  };

  describe("Read tasks", () => {
    it("get an empty list of tasks", async () => {
      const data = await api.getTasks();
      expect(data).toEqual({ tasks: [] });
    });
    it("get a list of tasks", async () => {
      await createTasks(3);
      const data = await api.getTasks();
      expect(data).toEqual({
        tasks: [
          {
            name: "Task1",
            description: "desc1",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "Task2",
            description: "desc2",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "Task3",
            description: "desc3",
            id: expect.any(String),
            complete: false,
          },
        ],
      });
    });
    it("get a single task from a list of 3", async () => {
      await createTasks(3);
      const data = await api.getSpecificTask(2);
      expect(data).toEqual({
        name: "Task2",
        description: "desc2",
        id: expect.any(String),
        complete: false,
      });
    });
    it("get non-existent task from list", async () => {
      const data = await api.getSpecificTask(999);
      expect(data).toEqual({});
    });
  });

  describe("Create tasks", () => {
    it("add one task", async () => {
      api.createTask(new Task("Task1", "Desc1"));
      const data = await api.getTasks();
      expect(data).toEqual({
        tasks: [
          {
            name: "Task1",
            description: "Desc1",
            id: expect.any(String),
            complete: false,
          },
        ],
      });
    });
  });

  describe("Delete tasks", () => {
    it("deletes one task from a list of 3", async () => {
      await createTasks(3);

      await api.deleteTask("2");
      const dataAfterDeletion = await api.getTasks();
      expect(dataAfterDeletion).toEqual({
        tasks: [
          {
            name: "Task1",
            description: "desc1",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "Task3",
            description: "desc3",
            id: expect.any(String),
            complete: false,
          },
        ],
      });
    });
    it("delete non-existent task from list", async () => {
      const response = await api.deleteTask("999");
      expect(response).toEqual(false);
    });
  });

  describe("Update tasks", () => {
    it("update task name of 1 task from a list of 3", async () => {
      await createTasks(3);

      const data = await api.getSpecificTask(2);
      await api.updateTask(data, { name: "NewName" });
      const dataAfterUpdate = await api.getTasks();
      expect(dataAfterUpdate).toEqual({
        tasks: [
          {
            name: "Task1",
            description: "desc1",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "NewName",
            description: "desc2",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "Task3",
            description: "desc3",
            id: expect.any(String),
            complete: false,
          },
        ],
      });
    });
    it("update task description of 1 task from list of 3", async () => {
      await createTasks(3);
      const data = await api.getSpecificTask(2);
      await api.updateTask(data, {
        description: "changedDesc",
      });
      const dataAfterUpdate = await api.getTasks();
      expect(dataAfterUpdate).toEqual({
        tasks: [
          {
            name: "Task1",
            description: "desc1",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "Task2",
            description: "changedDesc",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "Task3",
            description: "desc3",
            id: expect.any(String),
            complete: false,
          },
        ],
      });
    });
    it("update name and description of 1 task from list of 3", async () => {
      await createTasks(3);
      const data = await api.getSpecificTask(2);
      await api.updateTask(data, {
        name: "changedName",
        description: "changedDesc",
      });
      const dataAfterUpdate = await api.getTasks();
      expect(dataAfterUpdate).toEqual({
        tasks: [
          {
            name: "Task1",
            description: "desc1",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "changedName",
            description: "changedDesc",
            id: expect.any(String),
            complete: false,
          },
          {
            name: "Task3",
            description: "desc3",
            id: expect.any(String),
            complete: false,
          },
        ],
      });
    });
    it("update a non-existent task from list", async () => {
      const response = await api.updateTask(
        {},
        { name: "invalid", description: "invalid" }
      );
      expect(response).toEqual({ error: "Failed to update" });
    });
    it("update complete status of task", async () => {
      await createTasks(1);
      const data = await api.getSpecificTask(1);
      await api.updateTask(data, {
        complete: true,
      });
      const dataAfterUpdate = await api.getTasks();
      expect(dataAfterUpdate).toEqual({
        tasks: [
          {
            name: "Task1",
            description: "desc1",
            id: expect.any(String),
            complete: true,
          },
        ],
      });
    });
  });
});
