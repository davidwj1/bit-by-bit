const MongoTaskStorage = require("../repository/mongo-storage");
const Task = require("../repository/task.js");

let taskRepository;
beforeEach(async () => {
  taskRepository = new MongoTaskStorage();
  await taskRepository.setup();
});

afterEach(async () => {
  await taskRepository.cleanUp();
});

describe("Mongo Storage Test", () => {
  describe("Getting Tasks", () => {
    it("Return all tasks", async () => {
      const data = await taskRepository.getAll();
      expect(data).toEqual({ tasks: [] });
    });
    it("Return all tasks after creating 2", async () => {
      await taskRepository.create(new Task("Test1", "Desc1", "1"));
      await taskRepository.create(new Task("Test2", "Desc2", "2"));
      const data = await taskRepository.getAll();
      expect(data).toEqual({
        tasks: [
          { name: "Test1", description: "Desc1", id: "1", complete: false },
          { name: "Test2", description: "Desc2", id: "2", complete: false },
        ],
      });
    });
  });

  describe("Creating Tasks", () => {
    it("Create 1 task", async () => {
      await taskRepository.create(new Task("Test1", "Desc1", "1"));
      const data = await taskRepository.getAll();
      expect(data).toEqual({
        tasks: [
          { name: "Test1", description: "Desc1", id: "1", complete: false },
        ],
      });
    });
    it("Create 3 tasks", async () => {
      await taskRepository.create(new Task("Test1", "Desc1", "1"));
      await taskRepository.create(new Task("Test2", "Desc2", "2"));
      await taskRepository.create(new Task("Test3", "Desc3", "3"));
      const data = await taskRepository.getAll();
      expect(data).toEqual({
        tasks: [
          { name: "Test1", description: "Desc1", id: "1", complete: false },
          { name: "Test2", description: "Desc2", id: "2", complete: false },
          { name: "Test3", description: "Desc3", id: "3", complete: false },
        ],
      });
    });
  });

  describe("Deleting Tasks", () => {
    it("Delete 1 task out of 1", async () => {
      await taskRepository.create(new Task("Test1", "Desc1", "1"));
      await taskRepository.delete(1);
      const dataAfterDeletion = await taskRepository.getAll();
      expect(dataAfterDeletion).toEqual({
        tasks: [],
      });
    });
    it("Delete 2nd task out of 3", async () => {
      await taskRepository.create(new Task("Test1", "Desc1", "1"));
      await taskRepository.create(new Task("Test2", "Desc2", "2"));
      await taskRepository.create(new Task("Test3", "Desc3", "3"));
      await taskRepository.delete(2);
      const dataAfterDeletion = await taskRepository.getAll();
      expect(dataAfterDeletion).toEqual({
        tasks: [
          { id: "1", name: "Test1", description: "Desc1", complete: false },
          { id: "3", name: "Test3", description: "Desc3", complete: false },
        ],
      });
    });
    it("Delete non-existent task out of 3", async () => {
      await taskRepository.create(new Task("Test1", "Desc1", "1"));
      await taskRepository.create(new Task("Test2", "Desc2", "2"));
      await taskRepository.create(new Task("Test3", "Desc3", "3"));
      await taskRepository.delete(123456);
      const data = await taskRepository.getAll();
      expect(data).toEqual({
        tasks: [
          { id: "1", name: "Test1", description: "Desc1", complete: false },
          { id: "2", name: "Test2", description: "Desc2", complete: false },
          { id: "3", name: "Test3", description: "Desc3", complete: false },
        ],
      });
    });
  });

  /*Todo: add tests for updating; 
  desc,
  complete 
  and varying combos

  await taskRepository.update(newTask,{updatedName: "TestChange", updatedDescription: "ChangeDesc", updatedComplete: true});
  */
  describe("Updating Tasks", () => {
    it("Update a task name out of 1", async () => {
      const newTask = new Task("Test1", "Desc1", "1");
      await taskRepository.create(newTask);
      newTask.name = "TestChange";
      await taskRepository.update(newTask);
      const data = await taskRepository.getAll();
      expect(data).toEqual({
        tasks: [
          {
            id: "1",
            name: "TestChange",
            description: "Desc1",
            complete: false,
          },
        ],
      });
    });
    it("Update 2nd task out of 3", async () => {
      await taskRepository.create(new Task("Test1", "Desc1", "1"));
      const newTask = new Task("Test2", "Desc2", "2");
      await taskRepository.create(newTask);
      await taskRepository.create(new Task("Test3", "Desc3", "3"));
      newTask.name = "TestChange";
      newTask.description = "DescChange";
      await taskRepository.update(newTask);
      const data = await taskRepository.getAll();
      expect(data).toEqual({
        tasks: [
          { id: "1", name: "Test1", description: "Desc1", complete: false },
          {
            id: "2",
            name: "TestChange",
            description: "DescChange",
            complete: false,
          },
          { id: "3", name: "Test3", description: "Desc3", complete: false },
        ],
      });
    });
  });
});
