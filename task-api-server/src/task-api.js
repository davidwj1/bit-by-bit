const Task = require("./repository/task");

class TaskAPI {
  constructor(taskRepository) {
    this.repository = taskRepository;
  }

  async getTasks() {
    return await this.repository.getAll();
  }

  async getSpecificTask(id) {
    return await this.repository.getById(id);
  }

  async createTask(task) {
    const newTask = new Task(task.name, task.description, task.id);
    await this.repository.create(newTask);
  }

  async deleteTask(id) {
    const res = await this.repository.delete(id);
    return res;
  }

  async searchByName(name) {
    const res = await this.repository.searchByName(name);
    return res;
  }

  async updateTask(task, updates) {
    const apiResponse = await this.repository.update(task, updates);
    if (apiResponse === "fail") {
      return { error: "Failed to update" };
    }
  }
}

module.exports = TaskAPI;
