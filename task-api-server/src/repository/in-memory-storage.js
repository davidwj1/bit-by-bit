const TaskRepository = require("./task-repository");

class InMemoryStorage extends TaskRepository {
  constructor() {
    super();
    this.tasks = new Map();
    this.users = new Map();
  }

  async create(task) {
    this.tasks.set(task.id, task);
  }

  async getAll() {
    const iterator = this.tasks.values();
    return { tasks: Array.from(iterator) };
  }

  async getById(id) {
    let task;
    if (id !== undefined) {
      task = this.tasks.get(id.toString());
    }
    if (task !== undefined) {
      return task;
    }
    return {};
  }

  async searchByName(taskName) {
    const taskArray = [];
    for (let [key, value] of this.tasks.entries()) {
      if (value.name === taskName) {
        taskArray.push(value);
      }
    }
    return taskArray;
  }

  async update(task, updates) {
    const getTask = await this.getById(task.id);
    if (getTask !== null && JSON.stringify(getTask) !== "{}") {
      this.tasks.set(task.id, {
        name:
          "name" in updates && updates.name !== undefined
            ? updates.name
            : task.name,
        description:
          "description" in updates && updates.description !== undefined
            ? updates.description
            : task.description,
        id: task.id,
        complete:
          "complete" in updates && updates.complete !== undefined
            ? updates.complete
            : task.complete,
      });
      return "success";
    }
    return "fail";
  }

  async delete(id) {
    const deleteStatus = this.tasks.delete(id);
    return deleteStatus;
  }

  async cleanUp() {
    return "clean";
  }

  async setup() {
    return "setup";
  }

  async registerUser(newUser) {
    const user = this.users.get(newUser.username);
    if (user) {
      return undefined
    }
    this.users.set(newUser.username, { id: newUser.id, username: newUser.username, password: newUser.password })
    return { id: newUser.id, username: newUser.username, password: newUser.password }
  }

  async loginUser(user) {
    const loggedUser = this.users.get(user.username);
    return loggedUser
  }

}

module.exports = InMemoryStorage;
