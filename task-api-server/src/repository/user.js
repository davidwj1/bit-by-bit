const { v4: uuidv4 } = require("uuid");

class User {
    constructor(username, password, id = uuidv4()) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
}

module.exports = User;
