const TaskRepository = require("./task-repository");
const mongoose = require("mongoose");

class MongoStorage extends TaskRepository {
  constructor() {
    super();
  }

  async setup() {
    const dbName = process.env.MONGO_USER || "david";
    const dbPass = process.env.MONGO_PASSWORD || "david";
    const db = process.env.MONGO_INITDB_DATABASE || "tasks";
    const productionConnection = `mongodb://${dbName}:${dbPass}@mongo:27017/${db}`;
    const testingConnection = `mongodb://${dbName}:${dbPass}@localhost/${db}`;

    await mongoose.connect(productionConnection, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    this.TaskSchema = new mongoose.Schema({
      _id: String,
      name: String,
      description: String,
      complete: Boolean,
    });
    this.Task = mongoose.model("Task", this.TaskSchema);
  }

  convertToMongo(task) {
    if (task) {
      return {
        _id: task.id,
        name: task.name,
        description: task.description,
        complete: task.complete,
      };
    }
    return null;
  }

  convertFromMongo(task) {
    if (task) {
      return {
        id: task._id,
        name: task.name,
        description: task.description,
        complete: task.complete,
      };
    }
    return null;
  }

  async create(newTask) {
    const task = new this.Task(this.convertToMongo(newTask));
    await task.save();
  }

  async getAll() {
    const tasks = await this.Task.find();
    return { tasks: tasks.map(this.convertFromMongo).reverse() };
  }

  async getById(id) {
    const mongoTask = await this.Task.findById(id).exec();
    return this.convertFromMongo(mongoTask);
  }

  async searchByName(taskName) {
    const mongoTaskList = await this.Task.find({ name: taskName }).exec();
    const searchArrayResult = [];
    mongoTaskList.forEach((task) => {
      searchArrayResult.push(this.convertFromMongo(task));
    });
    return searchArrayResult;
  }

  async update(task,updates) {
    await this.Task.findByIdAndUpdate(task.id, updates);
  }

  async delete(id) {
    const deleteStatus = await this.Task.findByIdAndRemove(id);
    if (deleteStatus !== null) {
      return true;
    }
    return false;
  }

  async cleanUp() {
    //await this.Task.deleteMany({});
    //mongoose.deleteModel(/.+/);
    //Delete above for production
    await mongoose.connection.close();
  }
}

module.exports = MongoStorage;
