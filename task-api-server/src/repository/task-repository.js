class TaskRepository {
  async create() {}
  async getAll() {}
  async getById() {}
  async update() {}
  async delete() {}
  async searchByName() {}
  async cleanUp() {}
}

module.exports = TaskRepository;
