const { v4: uuidv4 } = require("uuid");

class Task {
  constructor(name, description, id = uuidv4(),complete=false) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.complete = complete
  }
}

module.exports = Task;
