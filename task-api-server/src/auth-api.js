const User = require("./repository/user");
const bcrypt = require('bcrypt')

class AuthAPI {
    constructor(userRepository) {
        this.repository = userRepository;
    }

    async createUser(user) {
        const hashPassword = bcrypt.hashSync(user.password, 14)
        const newUser = new User(user.username, hashPassword, user.id)
        const response = await this.repository.registerUser(newUser)
        return response === undefined ? { status: "User Exists" } : { id: response.id, username: response.username }
    }

    async loginUser(user) {
        const response = await this.repository.loginUser({ username: user.username, password: user.password })
        if (response) {
            if (bcrypt.compareSync(user.password, response.password) === true) {
                return { id: response.id, username: response.username, status: "LoggedIn" }
            }
            return { status: "Unsuccessful Login" }
        }
        return {}

    }
}

module.exports = AuthAPI;